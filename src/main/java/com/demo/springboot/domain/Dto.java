package com.demo.springboot.domain;


import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class Dto {
    private String numbers;

    public String getNumbers() {
        return numbers;
    }
}
