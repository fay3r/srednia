package com.demo.springboot.rest;

import com.demo.springboot.service.AverageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/math")
public class Controler {
    private static final Logger LOOGGER = LoggerFactory.getLogger(Controler.class);

    @Autowired
    private AverageService operacje;

    @GetMapping(value = "/digits/{num}")
    public ResponseEntity<Map<String,Double>> dodawanie(@PathVariable String num){
        double x = operacje.average(num);
        Map <String,Double> back = new HashMap<>();
        back.put("avarage",x);

        return new ResponseEntity(back,HttpStatus.OK);
    }

}
