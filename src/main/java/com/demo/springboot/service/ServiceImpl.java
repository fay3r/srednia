package com.demo.springboot.service;

import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements AverageService {
    @Override
    public double average(String x) {
        double finalAvr=0;
        String[] table;
        table= x.split(",");
        for(int i=0;i<table.length;i++){
            finalAvr+=Integer.parseInt(table[i]);
        }
        finalAvr/=table.length;
        return finalAvr;
    }
}
